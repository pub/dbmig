<?php

namespace DbMig;

use League\CLImate\CLImate;
use PDO;

abstract class Operation extends Action
{
    const TRIGGER_BEFORE_IMPORT = 'before_import';
    const TRIGGER_AFTER_IMPORT = 'after_import';
    const TRIGGER_BEFORE_MIGRATIONS = 'before_migrations';
    const TRIGGER_AFTER_MIGRATIONS = 'after_migrations';

    /**
     * Defines when the operation should be triggered
     *
     * @var string
     */
    public $trigger = self::TRIGGER_BEFORE_IMPORT;

    /**
     * @var CLImate
     */
    protected $cli;

    public function __construct(CLImate $cli, PDO $sourceDb, PDO $destinationDb)
    {
        $this->cli = $cli;

        parent::__construct($sourceDb, $destinationDb);
    }

    /**
     * Called when the operation is ran
     *
     * @param array $args
     */
    public abstract function run(array $args);
}
