<?php

namespace DbMig;

use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

/**
 * Migrates a custom table, checks if entry still exists in parent table.
 */
abstract class CustomMigration extends Migration
{
    /**
     * Parent module table
     *
     * @var string
     */
    public $parentTable = '';

    protected function init()
    {
        if (empty($this->sourceTable)) {
            $this->sourceTable = "{$this->parentTable}_cstm";
        }

        parent::init();
    }

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        $q->innerJoin($this->parentTable,
            Conditions::make("`{$this->parentTable}`.`id` = `{$this->sourceTable}`.`id_c`")
                ->andWith("`{$this->parentTable}`.`deleted` = 0"));
    }

}
