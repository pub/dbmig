<?php

namespace DbMig;

use DbMig\Helpers\ConfigLoader;
use DbMig\Helpers\DbHelper;
use DbMig\Helpers\MigrationsHelper;
use DbMig\Helpers\OperationsHelper;
use League\CLImate\CLImate;

class Migrator
{
    /**
     * @var string
     */
    protected $workingDir;

    /**
     * @var CLImate
     */
    protected $cli;

    /**
     * @var ConfigLoader
     */
    protected $config;

    /**
     * @var bool
     */
    protected $truncateDestination = false;

    /**
     * @var DbHelper
     */
    protected $srcDb;

    /**
     * @var DbHelper
     */
    protected $dstDb;

    /**
     * @param string|null $workingDir - Working directory, falls back to getcwd()
     */
    public function __construct(string $workingDir = null)
    {
        $this->workingDir = $workingDir ?? getcwd();

        $this->cli = new CLImate();
        $this->cli->forceAnsiOn();

        $this->config = new ConfigLoader($this->workingDir);
    }

    /**
     * Enable database truncation when set on Migration
     */
    public function enableTruncation()
    {
        $this->truncateDestination = true;
    }

    /**
     * Run migrations
     *
     * @param array|null $migrations Limit the set of migrations to run to the provided names
     * @return bool True on success, false otherwise
     */
    public function run(array $migrations = null)
    {
        try {
            if (!$this->openConnections()) {
                return false;
            }

            $operationsDir = preg_replace("/\{APP_DIR\}/i", $this->workingDir, $this->config->oppDir);
            $oppHelper = new OperationsHelper($operationsDir, $this->srcDb->con, $this->dstDb->con);
            $oppHelper->load();

            $migrationsDir = preg_replace("/\{APP_DIR\}/i", $this->workingDir, $this->config->migDir);
            $migrationsHelper = new MigrationsHelper(
                $migrationsDir,
                $this->truncateDestination,
                $this->srcDb,
                $this->dstDb
            );

            if (!$migrationsHelper->load($migrations)) {
                return false;
            }

            $oppHelper->trigger(Operation::TRIGGER_BEFORE_IMPORT);

            $migrationsHelper->run($oppHelper);

            $oppHelper->trigger(Operation::TRIGGER_AFTER_IMPORT);

            return true;
        } catch (MigrationException $e) {
            $this->cli
                ->br(2)
                ->red()->border("!")
                ->bold()->red("Failed to run migrations")
                ->red()->border("!")
                ->red($e->getMessage())
                ->br(2)
                ->bold()->red("Trace:")
                ->red($e->getTraceAsString())
                ->red()->border("!");
        }

        return false;
    }

    /**
     * Open database connections
     *
     * @return bool True on success, false otherwise
     */
    protected function openConnections()
    {
        $this->srcDb = new DbHelper($this->config->dbSrcDsn, $this->config->dbSrcUser, $this->config->dbSrcPass);
        $this->dstDb = new DbHelper($this->config->dbDstDsn, $this->config->dbDstUser, $this->config->dbDstPass);

        return $this->srcDb->open() && $this->dstDb->open();
    }
}
