<?php

namespace DbMig\Helpers;


use DbMig\Migration;
use DbMig\MigrationException;
use DbMig\Operation;
use League\CLImate\CLImate;
use League\CLImate\TerminalObject\Dynamic\Progress;

class MigrationsHelper
{
    const SELECT_LIMIT = 1000;

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var DbHelper
     */
    protected $src;

    /**
     * @var DbHelper
     */
    protected $dst;

    /**
     * @var boolean
     */
    protected $truncateDst;

    /**
     * @var CLImate
     */
    protected $cli;

    /**
     * @var Migration[] | null
     */
    protected $migrations = null;

    /**
     * @param string $directory Directory to scan for migrations
     * @param boolean $truncateDst
     * @param DbHelper $src Source database connection
     * @param DbHelper $dst Destination database connection
     */
    public function __construct(string $directory, $truncateDst, DbHelper $src, DbHelper $dst)
    {
        $this->directory = $directory;
        $this->src = $src;
        $this->dst = $dst;
        $this->truncateDst = $truncateDst;

        $this->cli = new CLImate();
        $this->cli->forceAnsiOn();
    }

    /**
     * Load migrations
     *
     * @param array|null $only If given only these migrations are loaded
     * @return boolean False when no migrations found, otherwise true
     * @throws MigrationException
     */
    public function load(array $only = null)
    {
        $this->cli
            ->green()
            ->inline('Loading migrations... ');

        $path = preg_replace("/\{APP_DIR\}/i", realpath(__DIR__ . "/.."), $this->directory);

        if (!is_dir($path)) {
            $this->cli
                ->red("[FAILED, no such directory '$path']");

            return false;
        }

        if ($only) {
            $this->loadOnly($path, $only);
        } else {
            $this->loadAll($path);
        }

        $count = count($this->migrations);
        $this->cli
            ->green(" [DONE, found {$count} migrations]");

        return $count > 0;
    }

    /**
     * @param OperationsHelper $operationsHelper
     * @return boolean
     */
    public function run(OperationsHelper $operationsHelper)
    {
        $count = count($this->migrations);
        $pad = strlen($count);

        foreach ($this->migrations as $index => $migration) {
            try {
                sprintf("|%' {$pad}d|\n", -2);

                $this->cli
                    ->lightBlue()->bold()->border()
                    ->lightBlue()->bold(sprintf("[%' {$pad}d/%d] MIGRATING %s...", ($index + 1), $count, get_class($migration)))
                    ->lightBlue()->bold()->border();

                $operationsHelper->trigger(Operation::TRIGGER_BEFORE_MIGRATIONS, ['migration' => $migration]);

                $this->runMigration($migration);

                $operationsHelper->trigger(Operation::TRIGGER_AFTER_MIGRATIONS, ['migration' => $migration]);
            } catch (MigrationException $e) {
                $this->cli
                    ->red()
                    ->bold()
                    ->inline("Migration failed: ")
                    ->red($e->getMessage());

                return false;
            } catch (\Exception $e) {
                $this->cli
                    ->br(2)
                    ->red()->border("!")
                    ->bold()->red()->inline("Unhandled exception: ")
                    ->red(get_class($e))
                    ->bold()->red()->inline("Message: ")
                    ->red($e->getMessage())
                    ->br(2)
                    ->bold()->red("Trace:")
                    ->red($e->getTraceAsString())
                    ->red()->border("!");

                return false;
            }
        }

        return true;
    }

    /**
     * Load all migration in the given $path
     *
     * @param string $path
     * @throws MigrationException
     */
    protected function loadAll(string $path) {
        $dir = new \DirectoryIterator($path);
        foreach ($dir as $file) {
            if (!$file->isDot() && !$file->isDir() && strtolower($file->getExtension()) === 'php') {
                $class = preg_replace("/\.{$file->getExtension()}$/", '', $file->getBaseName());
                $this->migrations[] = $this->loadMigration($class, "$path/$file");
            }
        }
    }

    /**
     * Loads the given list of migrations
     *
     * @param string $path
     * @param array $migrations
     * @throws MigrationException
     */
    protected function loadOnly(string $path, array $migrations) {
        foreach ($migrations as $migration) {
            $file = $path . DIRECTORY_SEPARATOR . "$migration.php";

            if (!is_file($file)) {
                throw new MigrationException("'$file' is not a file");
            }

            $this->migrations[] = $this->loadMigration($migration, $file);
        }
    }

    /**
     * @param string $class - Class name of the migration
     * @param string $file - File path of the migration
     * @return Migration
     * @throws MigrationException
     */
    protected function loadMigration(string $class, string $file) {
        try {
            $this->require($file);

            if (!class_exists($class)) {
                throw new MigrationException("No class with name '$class' in '$file'");
            }

            if (!is_subclass_of($class, Migration::class)) {
                throw new MigrationException("Class '$class' ($file) must extend " . Migration::class);
            }

            /** @var Migration $opp */
            $migration = new $class($this->src->con, $this->dst->con);

            $this->cli
                ->bold()->inline(" ($class) ");

            return $migration;
        } catch (\ErrorException $e) {
            throw new MigrationException("Failed to load migration $file: {$e->getMessage()}", $e->getCode(), $e);
        }
    }

    /**
     * Run the given migration
     *
     * @param Migration $migration
     */
    protected function runMigration($migration)
    {
        if ($this->truncateDst && $migration->truncateDestinationTable) {
            $this->cli
                ->bold()
                ->inline("Truncating table `{$migration->destinationTable}` in destination database... ");

            $this->dst->truncateTable($migration->destinationTable);

            $this->cli
                ->green("[DONE]");
        }

        $this->cli
            ->bold()
            ->inline("Counting entries in source table `{$migration->sourceTable}`... ");

        $count = $this->src->count($migration->buildSelectCount());
        $this->cli
            ->green("$count rows.")
            ->br();

        if ($count == 0) {
            $this->cli
                ->bold('Nothing to migrate.')
                ->br();

            return;
        }

        /** @var Progress $progress */
        $progress = $this->cli
            ->progress($count);

        $blockProcessStart = -1;
        $selectQuery = $migration->buildSelect();
        $insertQuery = null;
        $start = 0;
        $progress->advance(0, sprintf("Fetching %d rows", static::SELECT_LIMIT));
        while ($rows = $this->src->select($selectQuery, $start, static::SELECT_LIMIT)) {
            $timeEstimate = '';
            if ($blockProcessStart > -1) {
                $timeEstimate = ' | Time left: ';

                $blockTime = microtime(true) - $blockProcessStart;
                $blockEst = (($count - $start) / static::SELECT_LIMIT) * $blockTime;

                $blockHour = intval($blockEst / 60 / 60);
                $blockMin = intval(($blockEst - ($blockHour * 60 * 60)) / 60);
                $blockSec = intval($blockEst - ($blockHour * 60 * 60) - ($blockMin * 60));

                if ($blockHour > 0) {
                    $timeEstimate .= "{$blockHour}h ";
                }

                if ($blockMin > 0) {
                    $timeEstimate .= "{$blockMin}m ";
                }

                $timeEstimate .= sprintf(
                    "%ds [%.2fs / block of %d items]",
                    $blockSec,
                    $blockTime,
                    static::SELECT_LIMIT
                );

            }

            $blockProcessStart = microtime(true);

            $rowCount = count($rows);

            // Prevents progress bar from throwing an Exception if the row count increases during migration
            if ($start + $rowCount > $count) {
                $count = $start + $rowCount;
                $progress->total($count);
            }

            for ($i = 0; $i < $rowCount; $i++) {
                $progress->advance(1, sprintf(
                    "Inserting rows into destination table `{$migration->destinationTable}`: %d/%d%s",
                    $start + $i + 1,
                    $count,
                    $timeEstimate
                ));

                $processedRow = $migration->processRow($rows[$i]);

                if (!$insertQuery) {
                    // Prepare insert statement
                    $insertQuery = $this->dst->prepareInsert(
                        $migration->destinationTable,
                        array_keys($processedRow),
                        $migration->updateDuplicates
                    );
                }

                $insertQuery->execute($processedRow);
            }

            $progress->advance(0, sprintf("Fetching next %d rows...", static::SELECT_LIMIT));
            $start += count($rows);
        }

        if ($count) {
            $this->cli->br();
        }

        $this->cli
            ->bold()->inline("Migration complete: ")
            ->green(($count) ? "All $count rows processed correctly" : "Nothing to migrate")
            ->br(2);
    }

    /**
     * require_once that throws ErrorException instead of fatal error
     *
     * @param string $file - File to require
     * @throws \ErrorException
     */
    protected function require(string $file) {
        set_error_handler(function ($errNo, $errStr, $errFile, $errLine) {
            throw new \ErrorException($errStr, $errNo, 0, $errFile, $errLine);
        });

        require_once $file;

        restore_error_handler();
    }
}
