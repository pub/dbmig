<?php

namespace DbMig\Helpers;

use Dotenv\Dotenv;

/**
 * Helper class to load .env files
 *
 * @package DbMig\Helper
 * @property string $dbSrcDsn
 * @property string $dbSrcUser
 * @property string $dbSrcPass
 * @property string $dbDstDsn
 * @property string $dbDstUser
 * @property string $dbDstPass
 * @property string $migDir
 * @property string $oppDir
 */
class ConfigLoader
{
    public function __construct(string $path)
    {
        $env = new Dotenv($path);
        $env->load();
    }

    public function __get($name)
    {
        return getenv(strtoupper(snakeCase($name)));
    }
}
