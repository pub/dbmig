<?php

if (!function_exists('snakeCase')) {
    function snakeCase($str)
    {
        return preg_replace_callback('/([A-Z])/', function ($c) {
            return "_" . strtolower($c[1]);
        }, $str);
    }
}
