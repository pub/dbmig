<?php

namespace DbMig\Helpers;

use Latitude\QueryBuilder\SelectQuery;
use League\CLImate\CLImate;
use PDO;

class DbHelper
{
    /**
     * @var PDO | null
     */
    public $con = null;

    /**
     * @var string
     */
    protected $dsn;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var string
     */
    protected $pass;

    /**
     * @var CLImate
     */
    protected $cli;

    public function __construct(string $dsn, string $user, string $pass)
    {
        $this->dsn = $dsn;
        $this->user = $user;
        $this->pass = $pass;

        $this->cli = new CLImate();
    }

    /**
     * Open connection
     *
     * @return bool True on success, false on failure
     */
    public function open()
    {
        $this->cli
            ->lightBlue()
            ->bold()
            ->inline("Connecting to database '$this->dsn'... ");

        try {
            $this->con = new PDO($this->dsn, $this->user, $this->pass, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

            $this->cli
                ->green("[SUCCESS]");

            return true;
        } catch (\Exception $e) {
            $this->cli
                ->red($e->getMessage());
        }

        return false;
    }

    /**
     * Execute count statement on given connection
     *
     * @param SelectQuery $query
     * @return int
     */
    public function count(SelectQuery $query)
    {
        if (!$this->con) {
            throw new \RuntimeException("Connection was not opened before executing query");
        }

        $stmt = $this->con->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetch()['count'];
    }

    /**
     * Perform a select on a table with the given connection
     *
     * @param SelectQuery $query Prepared select
     * @param int $start Position to start from
     * @param int $limit Amount of entries to fetch
     * @return array
     */
    public function select(SelectQuery $query, $start, $limit)
    {
        if (!$this->con) {
            throw new \RuntimeException("Connection was not opened before executing query");
        }

        $query->limit($limit);
        $query->offset($start);

        $stmt = $this->con->prepare($query->sql());
        $stmt->execute($query->params());

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Insert a row into the given database
     *
     * @param string $table
     * @param array $columns
     * @param bool $updateDuplicates
     * @return \PDOStatement
     */
    public function prepareInsert($table, array $columns, $updateDuplicates)
    {
        if (!$this->con) {
            throw new \RuntimeException("Connection was not opened before executing query");
        }

        $columnString = join(", ", $columns);

        $sql = "INSERT INTO `$table` ($columnString) VALUES (:" . join(", :", $columns) . ")";

        if ($updateDuplicates) {
            $sql .= " ON DUPLICATE KEY UPDATE ";

            for ($i = 0; $i < count($columns); $i++) {
                if ($i > 0) {
                    $sql .= ", ";
                }

                $sql .= "{$columns[$i]} = :{$columns[$i]}";
            }
        }

        return $this->con->prepare($sql);
    }

    /**
     * Truncate the table on the given connection
     *
     * @param string $table
     * @return bool
     */
    public function truncateTable($table)
    {
        if (!$this->con) {
            throw new \RuntimeException("Connection was not opened before executing query");
        }

        $stmt = $this->con->prepare("TRUNCATE TABLE `$table`");
        return $stmt->execute();
    }
}
