<?php

namespace DbMig\Helpers;

use DbMig\Operation;
use League\CLImate\CLImate;

class OperationsHelper
{
    /**
     * @var string
     */
    protected $directory;

    /**
     * @var \PDO
     */
    protected $src;

    /**
     * @var \PDO
     */
    protected $dst;

    /**
     * @var CLImate
     */
    protected $cli;

    /**
     * @var Operation[][]
     */
    protected $operations = [];

    public function __construct(string $directory, \PDO $src, \PDO $dst)
    {
        $this->directory = $directory;
        $this->src = $src;
        $this->dst = $dst;
        $this->cli = new CLImate();
    }

    public function load()
    {
        $this->cli
            ->lightBlue()
            ->bold()
            ->inline('Loading operations... ');

        if (!is_dir($this->directory)) {
            $this->cli
                ->bold()
                ->yellow("[SKIP, operations directory not found '$this->directory']");

            return;
        }

        $dir = new \DirectoryIterator($this->directory);
        $count = 0;
        foreach ($dir as $file) {
            if (!$file->isDot() && !$file->isDir() && strtolower($file->getExtension()) === 'php') {
                require_once($file->getPathname());

                $class = preg_replace("/\.{$file->getExtension()}$/", '', $file->getBaseName());
                if (class_exists($class) && is_subclass_of($class, Operation::class)) {
                    /** @var Operation $opp */
                    $opp = new $class($this->cli, $this->src, $this->dst);
                    $this->operations[$opp->trigger][] = $opp;

                    $this->cli
                        ->bold()->inline(" ($class) ");

                    $count++;
                }
            }
        }

        $this->cli
            ->green(" [DONE, found {$count} operations]");
    }

    public function trigger(string $trigger, $args = []) {
        if (!array_key_exists($trigger, $this->operations)) {
            return;
        }

        foreach ($this->operations[$trigger] as $operation) {
            $this->cli
                ->lightBlue()
                ->inline('Triggering operation ')
                ->bold(get_class($operation))
                ->br();

            $operation->run($args);
        }
    }
}
