<?php

namespace DbMig;

use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

/**
 * Preconfigured migration for audit tables
 *
 * @package DbMig\
 * @property string $parentTable
 */
abstract class AuditMigration extends Migration
{
    /**
     * Table where the parent_id field can be found.
     *
     * Ex.: For accounts_audit this would be accounts
     *
     * @var string
     */
    public $parentTable = "";

    /**
     * @inheritdoc
     */
    public $sourceColumns = [
        "id",
        "parent_id",
        "date_created",
        "created_by",
        "field_name",
        "data_type",
        "before_value_string",
        "after_value_string",
        "before_value_text",
        "after_value_text",
    ];

    protected function init()
    {
        if (empty($this->sourceTable)) {
            $this->sourceTable = "{$this->parentTable}_audit";
        }

        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        $q->innerJoin($this->parentTable,
            Conditions::make("`{$this->parentTable}`.`id` = `{$this->sourceTable}`.`parent_id`")
                ->andWith("`{$this->parentTable}`.`deleted` = ?", 0)
        );
    }
}
