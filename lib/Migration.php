<?php

namespace DbMig;

use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\Expression;
use Latitude\QueryBuilder\SelectQuery;
use PDO;

/**
 * Base migration class
 */
abstract class Migration extends Action
{
    /**
     * If true the destination table is truncated before starting migration
     * Migrator->enableTruncation() must also be called for this to work
     *
     * @var bool
     */
    public $truncateDestinationTable = false;

    /**
     * Table from the source database to select from
     *
     * @var string
     */
    public $sourceTable = "";

    /**
     * Columns to select from the source table, if none given all are selected
     *
     * @var string[]
     */
    public $sourceColumns = [];

    /**
     * Table into which the data should be inserted in the destination database.
     * If left empty $this->sourceTable is used.
     *
     * @var string
     */
    public $destinationTable = "";

    /**
     * Map field names from source (key) table to destination (value) table
     * If a field is not present the name stays the same
     *
     * @var array
     */
    public $fieldMapping = [];

    /**
     * If true ON DUPLICATE KEY UPDATE is added to the insert query
     *
     * @var bool
     */
    public $updateDuplicates = true;

    /**
     * List of columns to ignore from the source table
     *
     * @var array
     */
    public $ignoreSourceColumns = [];

    /**
     * Build select query to count total rows to be migrated
     *
     * @return SelectQuery
     */
    public function buildSelectCount()
    {
        $q = SelectQuery::make(Expression::make('COUNT(*) AS count'))
            ->from($this->sourceTable);

        return $this->initSelect($q);
    }

    /**
     * Build base select query
     *
     * @return SelectQuery
     */
    public function buildSelect()
    {
        $q = SelectQuery::make()
            ->from($this->sourceTable);

        if ($this->sourceColumns) {
            $columns = [];
            foreach ($this->sourceColumns as $column) {
                $columns[] = "{$this->sourceTable}.$column";
            }

            $q->columns(...$columns);
        } else {
            $q->columns("{$this->sourceTable}.*");
        }

        return $this->initSelect($q);
    }

    /**
     * Common query initialization for both buildSelect() and countSelectQuery()
     *
     * @param SelectQuery $q
     * @return SelectQuery
     */
    protected function initSelect(SelectQuery $q)
    {
        $where = Conditions::make();

        $this->extendSelect($q, $where);

        // Only include where clause if not empty
        if ($where->sql() !== "") {
            $q->where($where);
        }

        return $q;
    }

    /**
     * Extend select query and conditions, used for both buildSelect() and countSelectQuery()
     *
     * @param SelectQuery $q
     * @param Conditions $where
     */
    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        $where->with("`{$this->sourceTable}`.`deleted` = 0");
    }

    /**
     * Process the given row of data from the data source
     * If overwitten in child class don't forget to call parent!
     *
     * @param array $row One row from the source table (associative)
     * @return array Columns to insert into the destination table
     */
    public function processRow(array $row)
    {

        // Field mapping
        foreach ($this->fieldMapping as $srcKey => $destKey) {
            $row[$destKey] = $row[$srcKey];
            unset($row[$srcKey]);
        }

        foreach ($this->ignoreSourceColumns as $ignoreColumn) {
            unset($row[$ignoreColumn]);
        }

        return $row;
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        return null;
    }

    /**
     * Initialization process
     */
    protected function init()
    {
        if (empty($this->destinationTable)) {
            $this->destinationTable = $this->sourceTable;
        }

        //compare columns in source and destination table
        $sourceColumns = $this->getTableColumns($this->sourceDb, $this->sourceTable);
        $destinationColumns = $this->getTableColumns($this->destinationDb, $this->destinationTable);

        $columnsMissingInTheDestination = array_diff($sourceColumns, $destinationColumns); //fields that exist in source but are missing in the destination
        $this->ignoreSourceColumns = array_merge($this->ignoreSourceColumns, $columnsMissingInTheDestination); //extend ignoreSourceColumns value with columns existing in source but missing in the destination
        $this->ignoreSourceColumns = array_unique($this->ignoreSourceColumns, SORT_REGULAR);


    }

    /**
     * Get tables columns list
     * @param PDO $con
     * @param string $tablename
     * @return array
     */
    private function getTableColumns(PDO $con, string $tablename)
    {
        $query = SelectQuery::make(
            'COLUMN_NAME'
        )->from('information_schema.columns');

        $where = Conditions::make();
        $where->with("table_name = ?", $tablename);
        $where->with("table_schema = DATABASE()");
        $query->where($where);

        $sql = $query->sql();

        $stmt = $con->prepare($sql);
        $stmt->execute($query->params());

        $result = $stmt->fetchAll(PDO::FETCH_COLUMN);

        return $result;
    }
}
