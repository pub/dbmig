<?php

namespace DbMig;

use PDO;

abstract class Action
{
    /**
     * PDO connection to the source database
     *
     * @var PDO
     */
    protected $sourceDb;

    /**
     * PDO connection to the destination database
     *
     * @var PDO
     */
    protected $destinationDb;

    /**
     * Action constructor.
     *
     * @param PDO $sourceDb
     * @param PDO $destinationDb
     */
    public function __construct(PDO $sourceDb, PDO $destinationDb)
    {
        $this->sourceDb = $sourceDb;
        $this->destinationDb = $destinationDb;

        $this->init();
    }

    /**
     * Initialization process
     */
    protected function init()
    {
    }
}
