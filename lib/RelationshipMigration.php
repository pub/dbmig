<?php

namespace DbMig;

use Latitude\QueryBuilder\Conditions;
use Latitude\QueryBuilder\SelectQuery;

/**
 * Migration for a sugar relationship table
 *
 * @package DbMig\
 */
abstract class RelationshipMigration extends Migration
{
    /**
     * Singular name of the left hand side relationship bean
     *
     * @var string
     */
    public $lhSingular = "";

    /**
     * Plural name of the left hand side relationship bean. If not specified "${lhSingular}s" is used.
     *
     * @var string|null
     */
    public $lhPlural = null;

    /**
     * Left hand side ID column
     *
     * @var string
     */
    public $lhId;

    /**
     * Left hand side ID column for destination table
     *
     * @var string
     */
    public $lhDestinationId;

    /**
     * Table of the left hand side bean
     *
     * @var string
     */
    public $lhTable;

    /**
     * Singular name of the right hand side relationship bean
     *
     * @var string
     */
    public $rhSingular = "";

    /**
     * Plural name of the right hand side relationship bean. If not specified "${rhSingular}s" is used.
     *
     * @var string|null
     */
    public $rhPlural = null;

    /**
     * Right hand side ID column
     *
     * @var string
     */
    public $rhId;


    /**
     * Right hand side ID column for destination table
     *
     * @var
     */
    public $rhDestinationId;

    /**
     * Table of the right hand side bean
     *
     * @var string
     */
    public $rhTable;

    /**
     * Some relationship tables contain additional columns. Specify them here.
     *
     * @var string[]
     */
    public $additionalSourceColumns = [];

    protected function init()
    {
        if (!is_string($this->lhId)) {
            $this->lhId = "{$this->lhSingular}_id";
        }

        if (!is_string($this->rhId)) {
            $this->rhId = "{$this->rhSingular}_id";
        }

        if (!is_string($this->lhPlural)) {
            $this->lhPlural = "{$this->lhSingular}s";
        }

        if (!is_string($this->rhPlural)) {
            $this->rhPlural = "{$this->rhSingular}s";
        }

        if (empty($this->sourceTable)) {
            $this->sourceTable = "{$this->lhPlural}_{$this->rhPlural}";
        }

        if (empty($this->lhTable)) {
            $this->lhTable = $this->lhPlural;
        }

        if (empty($this->rhTable)) {
            $this->rhTable = $this->rhPlural;
        }

        parent::init();
    }

    protected function extendSelect(SelectQuery $q, Conditions $where)
    {
        parent::extendSelect($q, $where);

        $q->innerJoin("{$this->lhTable} lhTable",
            Conditions::make("lhTable.`id` = `{$this->sourceTable}`.`{$this->lhId}` AND lhTable.`deleted` = 0")
        )->innerJoin("{$this->rhTable} rhTable",
            Conditions::make("rhTable.`id` = `{$this->sourceTable}`.`{$this->rhId}` AND rhTable.`deleted` = 0")
        );
    }

    public function __get($name)
    {
        $value = parent::__get($name);

        if (empty($value)) {
            if ($name == "sourceColumns") {
                $value = array_merge([
                    "id",
                    $this->lhId,
                    $this->rhId,
                    "date_modified",
                    "deleted",
                ], $this->additionalSourceColumns);
            }
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function processRow(array $row)
    {
        $row = parent::processRow($row);

        if (!empty($this->lhDestinationId)) {
            // The left hand id column name is not the same on destination, set new value, unset old
            $row[$this->lhDestinationId] = $row[$this->lhId];
            unset($row[$this->lhId]);
        }

        if (!empty($this->rhDestinationId)) {
            // The right hand id column name is not the same on destination, set new value, unset old
            $row[$this->rhDestinationId] = $row[$this->rhId];
            unset($row[$this->rhId]);
        }

        return $row;
    }
}
