<?php

namespace DbMig;

use RuntimeException;

class MigrationException extends RuntimeException
{
}
