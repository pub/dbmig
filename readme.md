# MySQL Database Migrator Library

## Example use

Install via composer, then use as follows:

    $migrator = new Migrator();
    
    // The migrator will use getcwd() to determine the location 
    // of the .env file
    // But you can also specify the location manually if required
    // $migrator = new Migrator(__DIR__ . "/..");
    
    // Optionally enable table truncation if required
    $migrator->enableTruncation();
    
    // Run migrations
    $migrator->run();
    
    // You can run only certain migrations by passing their class
    // names to the run() method
    // $migrator->run(["MyMigration", "MyOtherMigration"]

## Migration

A migration class must extend one of the following classes:

- DbMig\Migration
- DbMig\AuditMigration
- DbMig\CustomMigration

## Operations

Operations allow for the execution of custom code at certain points during the migration.

### Triggers

Determine when the operation is executed:

- **TRIGGER_BEFORE_IMPORT** - Before migrations are run
- **TRIGGER_AFTER_IMPORT** - After all migration ran successfully
- **TRIGGER_BEFORE_MIGRATIONS** - Before each migration
- **TRIGGER_AFTER_MIGRATIONS** - After each migration
